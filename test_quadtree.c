/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Charles CHAUDET Bertrand JAMAIN <bertrand.jamain@ecole.ensicaen.fr> 
 * @version 0.0.1
 */

/**
 * @file testQuadTree.c
 *  
 * Description of the program objectives.
 * All necessary references.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "quadtree.h"

#define SEUIL 100 

#define NB_TEST 10
#define NUM_COMMANDS 2

/********** FIRST PART **********/

int testCreateQuadtree() {
   int valid = 0;
   Quadtree quadtree = create_quadtree();
   int i;

   valid &= quadtree->M0 == 0;
   valid &= quadtree->sons[3] == NULL;
   for(i=0;i<3;i++){
      valid &= quadtree->sons[i] == NULL;
      valid &= quadtree->M1[i] == 0;
      valid &= quadtree->M2[i] == 0;
   }
   return valid;
}

int testWithImage(int seuil,char* title,char* nameImg){
   int i = 1;

   printf("%s\n", title);
   printf("Création de l'image...\n");

   image self = FAIRE_image();
   unsigned char blue[3] = {150,150,255};
   Quadtree tree;

   printf("Chargement...\n");
   
   i &= image_charger(self,title);
   printf("Découpage de l'image...\n");
   
   tree = split_image(self,seuil);

   printf("Tracé de l'arbre...\n");
   
   draw_quadtree(self,tree,blue);
   
   image_sauvegarder(self,nameImg);

   printf("Libération mémoire...\n");
   DEFAIRE_image(self);
   delete_quadtree(tree);

   tree = NULL;
   free(self);
   self=NULL;
   
   printf("Done !\n");
   return i;
}

/************** SECOND PART *************/

void test_approche_mixte(image img, int h) {
   Quadtree tree = NULL;
   unsigned char blue[3] = {150,150,255};

   printf("Creating...\n");
   tree = create_default_quadtree(h);
   printf("Initialisation...\n");
   init_quadtree(tree, img);
   printf("Updating...\n");
   update_quadtree(tree, img, 100);

draw_quadtree(img,tree,blue);
   
      image_sauvegarder(img,"testCharlemix.ppm");

   delete_quadtree(tree);
   tree = NULL;

   printf("Done !\n");
}

void test_approche_desc(image img) {
   Quadtree tree;
   unsigned char blue[3] = {150,150,255};

   tree = split_image(img, SEUIL);

      draw_quadtree(img,tree,blue);
   
      image_sauvegarder(img,"testCharledesc.ppm");
   delete_quadtree(tree);
   tree = NULL;

   printf("Done !\n");
}

/**************** MAIN *******************/

int main(int argc, char* argv[]) {
   int i=1;
   int k; 

   image self = FAIRE_image();
   int h[NB_TEST] = {1,2,3,4,5,6,7,8,9,10};
   int t[NB_TEST] = {0,0,0,0,0,0,0,0,0,0};
   int t1 = 0;
   int t2[NB_TEST] = {0,0,0,0,0,00,0,0,0,0};
   clock_t start_time;

   FILE* f = fopen("gnu_temp.dat", "w");

   printf("\n");

   /*---------- Impression des quadtree -----------*/
   printf("************** Dividing some images ****************\n'split_image()'\n\n");

   printf("\n-------- Black&White images --------\n");
   printf("\n>>> Dividing fille.pgm...\n");
   i &= testWithImage(SEUIL,"../IMAGES/fille.pgm","../IMAGES/filleTestG.pgm");
   printf("\n>>> Dividing lenna.pgm...\n");
   i &= testWithImage(SEUIL,"../IMAGES/lenna.pgm","../IMAGES/lenaTestG.pgm");
   printf("\n>>> Dividing zelda.pgm...\n");
   i &= testWithImage(SEUIL,"../IMAGES/zelda.pgm","../IMAGES/zeldaTestG.pgm");
   printf("\n>>> Dividing deg.pgm...\n");
   i &= testWithImage(SEUIL,"../IMAGES/deg.pgm","../IMAGES/degTestG.pgm");

   printf("\n-------- Colored images ---------\n");
   printf("\n>>> Dividing fille.ppm...\n");   
   i &= testWithImage(SEUIL,"../IMAGES/fille.ppm","../IMAGES/filleTest.ppm");
   printf("\n>>> Dividing lenna.ppm...\n");
   i &= testWithImage(SEUIL,"../IMAGES/lenna.ppm","../IMAGES/lenaTest.ppm");
   printf("\n>>> Dividing zelda.ppm...\n");
   i &= testWithImage(SEUIL,"../IMAGES/zelda.ppm","../IMAGES/zeldaTest.ppm");
   printf("\n>>> Dividing deg.ppm...\n");
   i &= testWithImage(SEUIL,"../IMAGES/deg.ppm","../IMAGES/degTest.ppm");
   

   /*---------- Execution time ----------*/

   printf("\n************** Testing execution time ******************\n\n");
   
   printf("Loading image fille.ppm...\n");
   image_initialize(self,3,100,100);
   i &= image_charger(self,"../IMAGES/lenna.ppm");
   
   printf("\n--------- Testing descending method ---------\n'init_quadtree()'\n'update_quadtree()'\n\n");
   start_time = clock();
   test_approche_desc(self);
   t1 = (float)(clock()-start_time);

   printf("\n--------- Testing mixt method ---------\n'split_image()'\n\n");
   for (k=0; k<NB_TEST; k++) {
      start_time = clock();
      printf("\n>>> Initial tree height : %d \n", h[k]);
      test_approche_mixte(self, h[k]);
      t[k] = (float)(clock()-start_time);
      t2[k] = t1;
   } 

   DEFAIRE_image(self);
   free(self);
   self=NULL;

   for (k = 0; k < NB_TEST; ++k) {
      fprintf(f, "%d %d %d\n", h[k], t2[k], t[k]);
   }
   fclose(f);

   system("gnuplot -p \"plot 'gnu_temp.dat' using 2 title 'Execution time for descending method' with lines,'gnu_temp.dat' using 3 title 'Execution time for mixt method' with lines\"");

   return 1;
}

