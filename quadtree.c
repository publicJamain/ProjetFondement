/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Charles CHAUDET Bertrand JAMAIN <jamain@ecole.ensicaen.fr>  
 * @version     0.0.1 
 */

/**
 * @file quadtree.c
 *  
 * @TO_DO stope quadtree <0 interdie
 * Define Structur Quadtree.
 * All necessary references.
 */

#include <stdio.h>

#include "quadtree.h"
#include "image_util.h"
#define DEBUG_VARIANCE 0
#define DEBUG_SPLIT 0


/*--------------------> Private <-------------------*/

void _draw_quadtree(image img, Quadtree tree, unsigned char* color,int xmin,int xmax,int ymin,int ymax);
void _split_image(image img, Quadtree tree, double seuil,int xmin,int xmax,int ymin,int ymax);
void _init_quadtree(Quadtree tree, image img, int xmin,int xmax,int ymin,int ymax);
void _update_quadtree(Quadtree tree, image img, double seuil, int xmin,int xmax,int ymin,int ymax);
double _give_variance(Quadtree tree, image img);
/* 
 * int* _sub_coord(int i,int* coord) 
 */

/*--------------------> MAIN FUNCTIONS <-------------------*/


  /***************/
 /*    Q3.1     */
/***************/

/**
 * Initialise a tree and his sons.
 *
 * @return A pointer on a tree.
 */
Quadtree create_quadtree() {
   Quadtree tree = (Quadtree) malloc(sizeof(struct quadtree));
   int i;
   
   for(i = 0; i < 4; i++) { 
      tree->sons[i]=NULL;
   }

   tree->M0 = 0;
   for(i = 0; i < 3; i++) {
      tree->M1[i]=0;
   }
   for(i = 0; i < 3; i++) {
      tree->M2[i]=0;
   }

   return tree;
}

/**
 * Transform a leaf into a branch by giving him 4 leafs.
 *
 * @param A leaf of the tree.
 */
void quadtree_subdivide(Quadtree tree) {
   int i=4;

   while(i--) {
      tree->sons[i] = create_quadtree();
   }
}

/**
 * Recursivly delete a tree, leaf by leaf.
*
 * @param If it's a leaf (all sons are NULL) then free this element.
 */
void delete_quadtree(Quadtree tree) {
  int i;

   for(i = 0; i < 4; i++) {
      if(tree->sons[i]) {
         delete_quadtree(tree->sons[i]);
      }
   }
   free(tree); /* warning because of the prototype the pointer was not disalow 4 Octets leak */
   tree=NULL;
}

  /***************/
 /*    Q3.2     */
/***************/

/**
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param Variance limit. If computed variance > seuil, then you need to split again.
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
  */
Quadtree split_image(image img, double seuil) {
   Quadtree tree = create_quadtree();
   _split_image(img,tree,seuil,0,image_give_largeur(img),0,image_give_hauteur(img));
   return tree;
}

/**
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param The 'spliting' of the image
 * @param The squares color.
  */
void draw_quadtree(image img, Quadtree tree, unsigned char* color) {
   if(tree!=NULL){
      _draw_quadtree(img,tree,color,0,image_give_largeur(img),0,image_give_hauteur(img));
   }
}

  /***************/
 /*    Q3.3     */
/***************/

/**
 * Create a tree with a fixed height
 *
 * @param The tree to intialize
 * @param The corresponding image
 */
Quadtree create_default_quadtree(int h) {
   int i;
   Quadtree tree = NULL;
   
   if (h > 0){
      tree = create_quadtree();
      for (i = 0; i < 4; i++) {
         tree->sons[i] = create_default_quadtree(h-1);
      }
   }
   return tree;
}

/**
 * init the moment of a tree
 *
 * @param The tree to intialize
 * @param The corresponding image
 */
void init_quadtree(Quadtree tree, image img) {
   _init_quadtree(tree, img, 0,image_give_largeur(img),0,image_give_hauteur(img));
}

/**
 * Split the node if variance > seuil else remove children node of the node "tree"
 *
 * @param The tree to initialize
 * @param The corresponding image
 * @param The threshold when you stop the subdivition
 */
void update_quadtree(Quadtree tree, image img, double seuil) {
   _update_quadtree(tree, img, seuil, 0,image_give_largeur(img),0,image_give_hauteur(img));
}

/*--------------------> OTHER FUNCTIONS <-------------------*/

/**
Private
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param The 'spliting' of the image
 * @param The squares color.
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
  */
void _draw_quadtree(image img, Quadtree tree, unsigned char* color,int xmin,int xmax,int ymin,int ymax) {
   
   if(tree->sons[0]){
      _draw_quadtree(img,tree->sons[0],color,xmin,(xmax+xmin)/2,ymin,(ymax+ymin)/2);
   }if(tree->sons[1]){
      _draw_quadtree(img,tree->sons[1],color,(xmin+xmax)/2,xmax,ymin,(ymax+ymin)/2);
   }if(tree->sons[2]){
      _draw_quadtree(img,tree->sons[2],color,xmin,(xmax+xmin)/2,(ymax+ymin)/2,ymax);
   }if(tree->sons[3]){
      _draw_quadtree(img,tree->sons[3],color,(xmin+xmax)/2,xmax,(ymin+ymax)/2,ymax);
   }
   draw_square(img,xmin,xmax,ymin,ymax,color);   
}

/**
Private
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param Variance limit. If computed variance > seuil, then you need to split again.
  */
void _split_image(image img,Quadtree tree, double seuil,int xmin,int xmax,int ymin,int ymax) {
   #if DEBUG_SPLIT != 0
   printf("x:%d, XM:%d, y:%d, yM:%d\t",xmin,xmax,ymin,ymax);
   #endif
   give_moments(img,xmin,xmax,ymin,ymax,&tree->M0,tree->M1,tree->M2);
   #if DEBUG_SPLIT != 0
   printf("M0%f M1%f M2%f\n",tree->M0,tree->M1[0],tree->M2[0]);
   #endif
   
   if(_give_variance(tree,img)>seuil){
      
      quadtree_subdivide(tree);

         _split_image(img,tree->sons[0],seuil,xmin,(xmax+xmin)/2,ymin,(ymax+ymin)/2);
         _split_image(img,tree->sons[1],seuil,(xmin+xmax)/2,xmax,ymin,(ymax+ymin)/2);
         _split_image(img,tree->sons[2],seuil,xmin,(xmax+xmin)/2,(ymax+ymin)/2,ymax);
         _split_image(img,tree->sons[3],seuil,(xmin+xmax)/2,xmax,(ymin+ymax)/2,ymax);


   }
}

/**
Private
 * init the moment of a tree
 *
 * @param The tree two initialize
 * @param The corresponding image
 * @param A table of coordinates in the image
 */
void _init_quadtree(Quadtree tree, image img, int xmin,int xmax,int ymin,int ymax) {
   int i, j;

   /* One son implie 4 sons */ 
   if (tree->sons[0]) {
     
      /* _init_quadtree(tree->sons[i], img, _sub_coord(i, coord)); */
         _init_quadtree(tree->sons[0],img,xmin,(xmax+xmin)/2,ymin,(ymax+ymin)/2);
         _init_quadtree(tree->sons[1],img,(xmin+xmax)/2,xmax,ymin,(ymax+ymin)/2);
         _init_quadtree(tree->sons[2],img,xmin,(xmax+xmin)/2,(ymax+ymin)/2,ymax);
         _init_quadtree(tree->sons[3],img,(xmin+xmax)/2,xmax,(ymin+ymax)/2,ymax);

            
      for (i=0; i< 4; i++) {
         tree->M0 += tree->sons[i]->M0;
         for (j=0; j<image_give_dim(img); j++) {
             tree->M1[j] += tree->sons[i]->M1[j];
             tree->M2[j] += tree->sons[i]->M2[j];
         }
      } 
   }else {
      give_moments(img, xmin, xmax, ymin, ymax, &tree->M0, tree->M1, tree->M2);
   }
}

/**
Private
 * Split the node if variance > seuil else remove children node of the node "tree"
 *
 * @param The tree to initialize
 * @param The corresponding image
 * @param The threshold when you stop the subdivition
 * @param A list of coordinates in the image
 */
void _update_quadtree(Quadtree tree, image img, double seuil, int xmin,int xmax,int ymin,int ymax) {
   int i;

   if (_give_variance(tree, img) > seuil) {
      /* If there is no sons */
      if (!tree->sons[0]) {
         quadtree_subdivide(tree);
         _init_quadtree(tree, img, xmin, xmax, ymin, ymax);
      }
      /*for (i = 0; i < 4; ++i) {
         _update_quadtree(tree->sons[i], img, seuil, _sub_coord(i, coord)); 
      }*/
      _update_quadtree(tree->sons[0],img,seuil,xmin,(xmax+xmin)/2,ymin,(ymax+ymin)/2);
      _update_quadtree(tree->sons[1],img,seuil,(xmin+xmax)/2,xmax,ymin,(ymax+ymin)/2);
      _update_quadtree(tree->sons[2],img,seuil,xmin,(xmax+xmin)/2,(ymax+ymin)/2,ymax);
      _update_quadtree(tree->sons[3],img,seuil,(xmin+xmax)/2,xmax,(ymin+ymax)/2,ymax);

   } else {

      /* One son implie 4 sons */    
      if (tree->sons[0]) {
         for (i = 0; i < 4; ++i) {
            delete_quadtree(tree->sons[i]);
            tree->sons[i]=NULL;
         }
      }
   }
}

/**
Private
 * Give the variance of one node.
 * 
 * @param The tree to initialize.
 * @param The corresponding image.
 */
double _give_variance(Quadtree tree, image img) {
   int i;
   double variance = 0;
   double var[] = {0,0,0};

   for(i = 0; i < image_give_dim(img); i++) {
      var[i] = (tree->M2[i]-((tree->M1[i]*tree->M1[i])/tree->M0));
   }

   variance = (var[0] + var[1] + var[2])/((image_give_dim(img)*tree->M0));

   #if DEBUG_VARIANCE != 0
      printf("v:%lf - M0:%lf M1:%lf M2:%lf \n",variance,tree->M0,tree->M1[0],tree->M2[0]);
   #endif

   return variance;
}


/**
 * Private
 */
/* IMPOSSIBLE DU AU PARTAGE MEMOIRE
int* _sub_coord(int i,int* coord) {

   int* cp_coord=(int*)malloc(sizeof(int)*4);
   
   if(i==1||i==2){
      cp_coord[0] = (coord[0]+coord[2])/2;
   }
   if(i==2||i==3){
      cp_coord[1] = (coord[1]+coord[3])/2;
   }
   if(i==0||i==3){
      cp_coord[2] = (coord[0]+coord[2])/2;
   }
   if(i==1||i==0){
      cp_coord[3] = (coord[1]+coord[3])/2;
   }

   return cp_coord;
}
*/