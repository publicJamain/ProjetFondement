/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 * 
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Auteur1 <jamain@ecole.ensicaen.fr>
 * @version     0.0.1 - Sep 20, 2010
 * 
 * @todo the list of improvements suggested for the file.
 * @bug the list of known bugs.
 */

#ifndef __QUADTREE_H
#define __QUADTREE_H


/**
 * @file quadtree.h
 *  
 * @RAPPORT Def 'feuille' (tous les fils NULL)
 */

#include <stdio.h>
#include <stdlib.h>
#include "image.h"


/** @RAPPORT */
typedef struct quadtree* Quadtree; /*CLASSE(quadtree);*/

struct quadtree {
   Quadtree sons[4];
   double M0,M1[3],M2[3];
};

/**
 * Initialise a tree and his sons.
 *
 * @return A pointer on a tree.
 */
extern Quadtree create_quadtree();

/**
 * Transform a leaf into a branch by giving him 4 leafs.
 *
 * @param A leaf of the tree.
 */
extern void quadtree_subdivide(Quadtree tree);

/**
 * Recursivly delete a tree, leaf by leaf.
*
 * @param If it's a leaf (all sons are NULL) then free this element.
 */
extern void delete_quadtree(Quadtree tree);

/**
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param Variance limit. If computed variance > seuil, then you need to split again.
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
  */
extern Quadtree split_image(image img, double seuil);

/**
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param The 'spliting' of the image
 * @param The squares color.
  */
void draw_quadtree(image img, Quadtree tree, unsigned char* color);

/**
 * Create a tree with a fixed height
 *
 * @param The tree to intialize
 * @param The corresponding image
 */
extern Quadtree create_default_quadtree(int h);

/**
 * init the moment of a tree
 *
 * @param The tree to intialize
 * @param The corresponding image
 */
void init_quadtree(Quadtree tree, image img);

/**
 * Split the node if variance > seuil else remove children node of the node "tree"
 *
 * @param The tree to initialize
 * @param The corresponding image
 * @param The threshold when you stop the subdivition
 */
void update_quadtree(Quadtree tree, image img, double seuil);




/*-------------------------------->PRIVATE<--------------------------------*/

/**
Private
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param The 'spliting' of the image
 * @param The squares color.
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
  */
void _draw_quadtree(image img, Quadtree tree, unsigned char* color,int xmin,int xmax,int ymin,int ymax);


/**
Private
 * Split the image into multiple little zones of low variance
 *
 * @param The image to split.
 * @param Variance limit. If computed variance > seuil, then you need to split again.
  */
void _split_image(image img,Quadtree tree, double seuil,int xmin,int xmax,int ymin,int ymax);

/**
Private
 * init the moment of a tree
 *
 * @param The tree to initialize
 * @param The corresponding image
 * @param A table of coordinates in the image
 */
void _init_quadtree(Quadtree tree, image img, int xmin,int xmax,int ymin,int ymax);

/**
Private
 * Split the node if variance > seuil else remove children node of the node "tree"
 *
 * @param The tree to initialize
 * @param The corresponding image
 * @param The threshold when you stop the subdivition
 * @param A list of coordinates in the image
 */
void _update_quadtree(Quadtree tree, image img, double seuil, int xmin,int xmax,int ymin,int ymax);

/**
Private
 * Give the variance of one node.
 * 
 * @param The tree to initialize.
 * @param The corresponding image.
 */
double _give_variance(Quadtree tree, image img);

#endif

