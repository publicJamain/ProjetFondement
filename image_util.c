/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author CHAUDET Charles <cchaudet@ecole.ensicaen.fr>
 * @author JAMAIN Bertrand <jamain@ecole.ensicaen.fr>  
 * @version     0.0.1
 * 
 */

/**
 * @file image_util.c
 *  
 * Description of the program objectives.
 * All necessary references.
 */

#define DEBUG_MOMENT 0
#define LIGHT_SQUARE 1
#include "image_util.h" 

/*----------------> Privates <------------------*/

void _draw_horizontal(image img, int xmin, int y, int xmax, unsigned char* color) ;
void _draw_vertical(image img, int x, int ymin, int ymax, unsigned char* color) ;


/*----------------> MAIN FUNCTIONS <-------------*/

/**
 * Draw a square by drawing both horizontals and both verticals
 *
 * @param Image to modify
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
 * @param line color
 */
extern void draw_square(image img, int xmin, int xmax, int ymin, int ymax, unsigned char* color) 
{
   
   _draw_horizontal(img, xmin, ymin, xmax, color);
   _draw_vertical(img, xmin, ymin, ymax, color);
   #if LIGHT_SQUARE == 0
   _draw_horizontal(img, xmin, ymax-1, xmax, color);
   _draw_vertical(img, xmax-1, ymin, ymax, color);
   #endif
}


/**
 * A complete description of the function.
 *
 * @param Image to modify
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
 *
 * @warning m0, m1, m2 should not be initialised to 0
 *
 * @RAPPORT m0 is casted in double (see quadtree definition)
 */
extern void give_moments(image img, int xmin, int xmax, int ymin, int ymax, double* m0, double* m1, double* m2) {
   Point point;
   int i, j, k;
   int* tab;


   for (i = xmin; i < xmax; i++){
      for (j = ymin; j < ymax; j++){
         *m0 += 1;
	      COORDX(point)=i;
	      COORDY(point)=j;
	      image_move_to(img, &point);
         tab = image_lire_pixel(img);
	 
         for (k = 0; k < image_give_dim(img); k++){
            m1[k] += tab[k];
            m2[k] += tab[k]*tab[k];
         }
         image_pixel_dessous(img);
      }
      image_pixel_droite(img);
   }
   #if DEBUG_MOMENT !=0
   printf("M0:%lf ->%d, %d, %d, %d, %d\n",m0[0],(xmax-xmin)*(ymax-ymin),xmin,xmax,ymin,ymax);
   #endif
}




/*--------------> OTHERS FUNCTIONS <-----------*/

/**
 * Draw an horizontal line
 *
 * @param Image to modify
 * @param First point x-coord
 * @param Line y-coord
 * @param Last point x-coord
 * @param line color
 */
void _draw_horizontal(image img, int xmin, int y, int xmax, unsigned char* color) {
   int i;
   Point point;
   
   COORDX(point)=xmin;
   COORDY(point)=y;
   
   image_move_to(img,&point);
   for (i = 0; i < xmax-xmin; ++i){
      image_ecrire_pixel(img, (int*)color);
      image_pixel_droite(img);
   }
}


/**
 * Draw a vertical line
 *
 * @param Image to modify
 * @param Column x-coord
 * @param First point y-coord
 * @param Last point y-coord
 * @param line color
 */
void _draw_vertical(image img, int x, int ymin, int ymax, unsigned char* color) {
   int i;
   Point point;
   
   COORDX(point)=x;
   COORDY(point)=ymin;
   
   image_move_to(img,&point);
   for (i = 0; i < ymax-ymin; ++i){
      image_ecrire_pixel(img, (int*)color);
      image_pixel_dessous(img);
   }
}
