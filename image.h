/*
 * @file image.h
 *
 * @brief Contains all the Image methods
 */

#ifndef IMAGE_H
#define IMAGE_H

#include "objet.h"
#include "classe.h"

#include "point.h"
#include "move_type.h"

/* Type de fonction a utiliser pour calculer la distance entre 2 pixel */
typedef double (*PFdist)(int*, int*, booleen);
 
CLASSE(image);

/*
 * CONSTRUCTOR & DESTRUCTOR
 */

/**
 * Constructor, instanciate an Image object
 *
 * @return An Image object.
 */
extern image FAIRE_image();
/**
 * Destructor, destroy an Image object.
 * 
 * @param self The image object.
 */
extern void DEFAIRE_image(image self);


/*
 * LOADING & SAVING
 */

/**
 * Loads an image from a given file name.
 *
 * @param self The image object.
 * @param filename Name of the file containing the image.
 *
 * @return A negative value if an error occured.
 */
extern int image_charger(image self, char *filename);

/**
 * Saves an image on the disk.
 *
 * @param self The image object.
 * @param filename Name of the file to be written on the disk.
 *
 * @return A negative value if an error occured.
 */
extern int image_sauvegarder(image self, char *filename);

/**
 * Writes an image to a given stream.
 *
 * @param self The image object.
 * @param stream The stream where to write the image data.
 *
 * @return A negative value if an error occured.
 */
extern int image_to_stream(image self, FILE *stream);


/*
 * IMAGE READING AND WRITING
 */

/**
 * Reads a pixel on an image at cursor's position.
 *
 * @param self The image object.
 *
 * @return A scalar if it's a grayscale image and an array with RGB values if
 * it is not.
 */
extern int* image_lire_pixel(image self);

/**
 * Writes a pixel on the image at cursor's position.
 *
 * @param self The image object.
 * @param pixel A scalar value if it's a greyscale image or an array with
 * RGB values if it is not.
 */
extern void image_ecrire_pixel(image self, int *pixel);

/**
 * Reads a pixel at a given position on an image.
 *
 * @param self The image object.
 * @param x The x coordinate of the pixel read.
 * @param y The y coordinate of the pixel to read.
 * @param pixel A scalar value if it's a greyscale image or an array with
 */
extern void image_read_pixel(image self, int x, int y, unsigned char *pixel);

/**
 * Writes a pixel at a given position on an image.
 *
 * @param self The image object.
 * @param x The x coordinate of the pixel read.
 * @param y The y coordinate of the pixel to read.
 * @param pixel A scalar value if it's a greyscale image or an array with
 */
extern void image_write_pixel(image, int, int, unsigned char*);


/*
 * IMAGE MOVEMENTS
 */

/**
 * Checks if there's a next pixel to read.
 *
 * @param self The image object.
 *
 * @return A boolean value set to True if there's a next pixel, else False.
 */
extern booleen image_pixel_suivant(image self);

/**
 * Set the image cursor at the beggining.
 *
 * @param self The image object
 */
extern void image_debut(image self);

/**
 * Move the image cursor to a given location.
 *
 * @param self The image object.
 * @param p The structure with the coordinates to move to.
 *
 * @return True if the cursor has successfully been moved, else False.
 */
extern booleen image_move_to(image self, point p);

/**
 * Move the image cursor above its current position (y + 1)
 *
 * @param self The image object.
 *
 * @return True if the cursor has successfully been moved, else False.
 */
extern booleen image_pixel_dessus(image self);

/**
 * Move the image cursor below its current position (y - 1)
 *
 * @param self The image object.
 *
 * @return True if the cursor has successfully been moved, else False.
 */
extern booleen image_pixel_dessous(image self);

/**
 * Move the image cursor on the left of its current position (x - 1)
 *
 * @param self The image object.
 *
 * @return True if the cursor has successfully been moved, else False.
 */
extern booleen image_pixel_gauche(image self);

/**
 * Move the image cursor on the right of its current position (x + 1)
 *
 * @param self The image object.
 *
 * @return True if the cursor has successfully been moved, else False.
 */
extern booleen image_pixel_droite(image self);

/**
 */
extern void image_set_distance(image self, PFdist);

/* img,dim,largeur,hauteur */

/**
 * Initializes an empty image.
 *
 * @param self The image object.
 * @param dim The dimension of the pixels (1 for greyscale, 3 for RGB colors)
 * @param width Width of the image.
 * @param height Height of the image.
 */
extern void image_initialize(image self, int dim, int width, int height);

/**
 */
extern double image_distance(image self, point p, move_type movet);


/*
 * GETTERS TO IMAGE PRIVATE VARIABLES
 */

/**
 * Get the image dimension.
 *
 * @param self The image object.
 * 
 * @return The image dimension.
 */
extern int image_give_dim(image);

/**
 * Get the image hauteur.
 *
 * @param self The image object.
 * 
 * @return The image hauteur.
 */
extern int image_give_hauteur(image);

/**
 * Get the image largeur.
 *
 * @param self The image object.
 * 
 * @return The image largeur.
 */
extern int image_give_largeur(image self);

#endif /* IMAGE_H */



