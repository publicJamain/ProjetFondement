CC=gcc
CFLAG=-Wall -g
LIBS=-L./
LIB_IMG=-limage64
OBJ=main.o image_util.o test_quadtree.o quadtree.o 


all: test_image test_quadtree

image_util.o: image_util.c image_util.h
	$(CC) $(CFLAG) -c $< 

test_quadtree.o: test_quadtree.c
	$(CC) $(CFLAG) -c test_quadtree.c

quadtree.o: quadtree.c
	$(CC) $(CFLAG) -c quadtree.c

test_image: main.o
	$(CC) $^ -o $@ $(LIBS) $(LIB_IMG) 

test_quadtree: quadtree.o test_quadtree.o image_util.o
	$(CC) $^ -o $@ $(LIBS) $(LIB_IMG) -g

main.o: main.c image.h objet.h exit_if.h classe.h point.h move_type.h type_obj.h
	$(CC) $(CFLAG) -c main.c 

clean:
	rm -f $(OBJ) test_quadtree test_image gnu_temp
