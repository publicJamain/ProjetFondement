#ifndef __IMAGE_UTIL_H
#define __IMAGE_UTIL_H

/* -*- c-basic-offset: 3 -*- 
;

 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 
/**
 * @author CHAUDET Charles <cchaudet@ecole.ensicaen.fr>
 * @author JAMAIN Bertrand <jamain@ecole.ensicaen.fr>  
 * @version     0.0.1
 * 
 */
/**
 * @file image_util.c
 *  
 * Description of the program objectives.
 * All necessary references.
 */


#include <stdio.h> 
#include "image.h"



/**
 * Draw a square by drawing both horizontals and both verticals
 *
 * @param Image to modify
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
 * @param line color
 */
extern void draw_square(image img, int xmin, int xmax, int ymin, int ymax, unsigned char* color);

/**
 * A complete description of the function.
 *
 * @param Image to modify
 * @param First point x-coord
 * @param Last point x-coord
 * @param First point y-coord
 * @param Last point y-coord
 *
 * @warning m0, m1, m2 should not be initialised to 0
 *
 * @RAPPORT m0 is casted in double (see quadtree definition)
 */
extern void give_moments(image img, int xmin, int xmax, int ymin, int ymax, double* m0, double* m1, double* m2);

/*----------------> Privates <------------------*/


/**
 * Draw an horizontal line
 *
 * @param Image to modify
 * @param First point x-coord
 * @param Line y-coord
 * @param Last point x-coord
 * @param line color
 */
void _draw_horizontal(image img, int xmin, int y, int xmax, unsigned char* color);

/**
 * Draw a vertical line
 *
 * @param Image to modify
 * @param Column x-coord
 * @param First point y-coord
 * @param Last point y-coord
 * @param line color
 */
void _draw_vertical(image img, int x, int ymin, int ymax, unsigned char* color);


#endif
